package main

import (
	"errors"
	"fmt"
	"net/textproto"
	"os"
	"strconv"
	"strings"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

const rootHelp = `
## Environment Variables

* SQUEEZECTRL_HOST - LMS Address
* SQUEEZECTRL_PORT - LMS Port
* SQUEEZECTRL_PLAYER - LMS Player
`

func main() {

	defaultHost := os.Getenv("SQUEEZECTRL_HOST")
	if defaultHost == "" {
		defaultHost = "localhost"
	}

	defaultPort := 9090
	portStr := os.Getenv("SQUEEZECTRL_PORT")
	if portStr != "" {
		p, err := strconv.Atoi(portStr)
		if err != nil {
			fmt.Println("Unable to convert SQUEEZECTRL_PORT to int", err)
			os.Exit(1)
		}
		defaultPort = p
	}

	defaultPlayer := os.Getenv("SQUEEZECTRL_PLAYER")

	rootCmd := &cobra.Command{
		Use:   "squeezectrl",
		Long:  rootHelp,
		Short: "Control your Logitech Media Server aka. Slimserver",
	}
	host := rootCmd.PersistentFlags().String("host", defaultHost, "LMS host")
	port := rootCmd.PersistentFlags().Int("port", defaultPort, "LMS Port")
	player := rootCmd.PersistentFlags().String("player", defaultPlayer, "LMS Player")

	addr := func() string { return fmt.Sprintf("%s:%d", *host, *port) }

	rootCmd.AddCommand(
		&cobra.Command{
			Use:   "pause",
			Short: "Pause the current player",
			Run: func(cmd *cobra.Command, args []string) {
				c := Must(Connect(addr()))
				defer c.Close()
				c.Pause(*player)
			},
		},
		&cobra.Command{
			Use:   "unpause",
			Short: "unpause the current player",
			Run: func(cmd *cobra.Command, args []string) {
				c := Must(Connect(addr()))
				defer c.Close()
				c.Unpause(*player)
			},
		},
		&cobra.Command{
			Use:   "toggle",
			Short: "toggle play/pause of the current player",
			Run: func(cmd *cobra.Command, args []string) {
				c := Must(Connect(addr()))
				defer c.Close()
				c.TogglePause(*player)
			},
		},
		&cobra.Command{
			Use:   "next",
			Short: "go to the next song",
			Run: func(cmd *cobra.Command, args []string) {
				c := Must(Connect(addr()))
				defer c.Close()
				c.NextSong(*player)
			},
		},
		&cobra.Command{
			Use:   "prev",
			Short: "go to the previous song",
			Run: func(cmd *cobra.Command, args []string) {
				c := Must(Connect(addr()))
				defer c.Close()
				c.NextSong(*player)
			},
		},
		&cobra.Command{
			Use:   "list",
			Short: "list all players",
			Run: func(cmd *cobra.Command, args []string) {
				c := Must(Connect(addr()))
				players := Must(c.Players())

				table := tablewriter.NewWriter(os.Stdout)
				table.SetBorder(false)
				table.SetHeader([]string{"ID", "Name", "IP", "Model"})

				for _, p := range players {
					table.Append([]string{p.Id, p.Name, p.Ip, p.Model})
				}
				table.Render()
			},
		},
		&cobra.Command{
			Use:   "search",
			Short: "Search for a Song",
			Run: func(cmd *cobra.Command, args []string) {
				c := Must(Connect(addr()))
				defer c.Close()
				c.Search("avaion")
			},
		},
	)
	volumeCmd := &cobra.Command{
		Use:   "volume <volume>",
		Short: "get/set volume",
		Long:  "volume 10 or volume 50",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				fmt.Println("Expected exactly 1 argument")
				os.Exit(1)
			}
			c := Must(Connect(addr()))
			defer c.Close()
			c.SetVolume(*player, args[0])
		},
	}
	rootCmd.AddCommand(volumeCmd)

	volumeCmd.AddCommand(&cobra.Command{
		Use:   "add <volume>",
		Short: "Add relative volume",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				fmt.Println("Expected exactly 1 argument")
				os.Exit(1)
			}
			c := Must(Connect(addr()))
			defer c.Close()
			c.SetVolume(*player, "+"+args[0])
		},
	})
	volumeCmd.AddCommand(&cobra.Command{
		Use:   "sub <volume>",
		Short: "Subtract relative volume",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				fmt.Println("Expected exactly 1 argument")
				os.Exit(1)
			}
			c := Must(Connect(addr()))
			defer c.Close()
			c.SetVolume(*player, "-"+args[0])
		},
	})

	seekCmd := &cobra.Command{
		Use:     "seek <time>",
		Example: "seek 55",
		Short:   "Seek to an absolute position in seconds",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				fmt.Println("Expected exactly 1 argument")
				os.Exit(1)
			}
			seconds := Must(strconv.Atoi(args[0]))
			c := Must(Connect(addr()))
			defer c.Close()
			c.Seek(*player, seconds)
		},
	}
	rootCmd.AddCommand(seekCmd)
	seekCmd.AddCommand(&cobra.Command{
		Use:     "forward <time>",
		Short:   "Seek to a relative position forward",
		Example: "forward 5",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				fmt.Println("Expected exactly 1 argument")
				os.Exit(1)
			}
			seconds := Must(strconv.Atoi(args[0]))
			c := Must(Connect(addr()))
			defer c.Close()
			c.SeekRelative(*player, seconds)
		},
	})

	seekCmd.AddCommand(&cobra.Command{
		Use:     "backward <time>",
		Short:   "Seek to a relative position backward",
		Example: "backward 5",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				fmt.Println("Expected exactly 1 argument")
				os.Exit(1)
			}
			seconds := Must(strconv.Atoi(args[0]))
			c := Must(Connect(addr()))
			defer c.Close()
			c.SeekRelative(*player, seconds*-1)
		},
	})

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	//players, err := c.Players()
	//if err != nil {
	//	panic(err)
	//}

	//for _, p := range players {
	//	if p.Name == "Stereoanlage" {
	//		c.TogglePause(p.Id)
	//	}
	//}

}

func Must[T any](v T, err error) T {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return v
}

func Connect(addr string) (*Client, error) {
	conn, err := textproto.Dial("tcp", addr)
	if err != nil {
		return nil, err
	}
	return &Client{conn: conn}, nil
}

type Client struct {
	conn *textproto.Conn
}

func (c *Client) Close() error {
	return c.conn.Close()
}

type PlayerInfo struct {
	Id        string
	Ip        string
	Name      string
	Model     string
	Connected bool
	IsPlaying bool
}

//func (c *Client) SetVolume(player string, volume int) error {
//
//}

func (c *Client) Search(term string) error {
	resp, err := c.call("search 0 20 term:%s", term)
	fmt.Println(err)
	fmt.Println(resp)
	return nil
}

func (c *Client) Seek(player string, seconds int) error {
	return c.cast("%s time %d", player, seconds)
}

func (c *Client) SeekRelative(player string, seconds int) error {
	direction := ""
	if seconds >= 0 {
		direction = "+"
	}
	return c.cast("%s time %s%d", player, direction, seconds)
}

func (c *Client) PrevSong(player string) error {
	return c.cast("%s playlist index -1", player)
}

func (c *Client) NextSong(player string) error {
	return c.cast("%s playlist index +1", player)
}

func (c *Client) Pause(player string) error {
	return c.cast("%s pause 1", player)
}
func (c *Client) Unpause(player string) error {
	return c.cast("%s pause 0", player)
}

func (c *Client) SetVolume(player, vol string) error {
	return c.cast("%s mixer volume %s", player, vol)
}

func (c *Client) TogglePause(player string) error {
	return c.cast("%s pause", player)
}

func (c *Client) cast(requestStr string, args ...any) error {
	_, err := c.call(requestStr, args...)
	return err
}

func (c *Client) call(requestStr string, args ...any) (string, error) {
	if err := c.conn.PrintfLine(requestStr, args...); err != nil {
		return "", err
	}
	return c.conn.ReadLine()
}

func (c *Client) Players() ([]*PlayerInfo, error) {
	resp, err := c.call("players 0 20")
	if err != nil {
		return nil, err
	}

	parts := strings.Split(resp, " ")

	players := []*PlayerInfo{}
	var currentPlayer *PlayerInfo = nil

	for _, p := range parts {
		if strings.Contains(p, "%3A") {
			d := strings.SplitN(p, "%3A", 2)
			if len(d) != 2 {
				return nil, errors.New("malformed data...")
			}
			key := d[0]
			value := strings.ReplaceAll(d[1], "%3A", ":")

			if key == "playerindex" {
				if currentPlayer != nil {
					players = append(players, currentPlayer)
				}
				currentPlayer = &PlayerInfo{}
			}

			switch key {
			case "playerid":
				currentPlayer.Id = value
			case "ip":
				currentPlayer.Ip = value
			case "name":
				currentPlayer.Name = value
			case "model":
				currentPlayer.Model = value
			case "connected":
				currentPlayer.Connected = value == "1"
			case "isplaying":
				currentPlayer.IsPlaying = value == "1"
			}
		}

	}
	if currentPlayer != nil {
		players = append(players, currentPlayer)
	}

	return players, nil

}
