# Squeezectrl

Control your Logitech Media Server from the commandline


## Example

```
> export SQUEEZECTRL_HOST=192.168.12.34
> export SQUEEZECTRL_PORT=9090

> squeezectrl list

         ID         |     NAME     |          IP          |    MODEL
--------------------+--------------+----------------------+--------------
  b8:cd:eb:16:45:ca | MusicBox123  | 192.168.12.34:46398  | squeezelite
  1f:7b:09:af:11:92 | Laptop       | 192.168.12.55:43522  | squeezelite

> export SQUEEZECTRL_PLAYER=1f:7b:09:af:11:92

> squeezectrl unpause
> squeezectrl volume add 10
```